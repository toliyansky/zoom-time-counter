console.info("Zoom Time Counter started")

/**
 * Еhe object in which the property has a key as a username, and the value is the amount of talk time.
 */
let userTimeMap = {}

const LAUNCH_MEETING_BUTTON_TEXTS = [
    "Launch Meeting",
    "Iniciar reunión",
    "Meeting eröffnen",
    "启动会议",
    "啟動會議",
    "Lancer la réunion",
    "Iniciar a reunião",
    "ミーティングを起動",
    "Запустить конференцию",
    "회의 시작",
    "Avvia riunione",
    "Khởi chạy cuộc họp"
]

const JOIN_FROM_BROWSER_LINK_TEXT = [
    "Join from Your Browser",
    "Únase desde su navegador",
    "Mit Ihrem Browser anmelden",
    "使用浏览器加入",
    "改用您的瀏覽器加入",
    "Rejoignez depuis votre navigateur",
    "Ingresse em seu navegador",
    "ブラウザから参加してください",
    "Войдите с помощью браузера",
    "브라우저에서 참가",
    "Entra dal browser",
    "Tham gia từ trình duyệt của bạn"
]

clickLaunchMeetingButton()
increaseSizeOfRedirectLink()
timeProcessing()

/**
 * Auto click on button "Launch Meeting".
 * Additional remove block with needless info.
 */
function clickLaunchMeetingButton() {
    try {
        let allDivs = document.getElementsByTagName("div")
        let launchMeetingDiv

        for (let div of allDivs) {
            if (LAUNCH_MEETING_BUTTON_TEXTS.some(text => div.textContent === text)) {
                launchMeetingDiv = div
                console.debug("Launch meeting button found")
                break
            }
        }

        if (launchMeetingDiv !== undefined) {
            launchMeetingDiv.click()
            launchMeetingDiv.parentNode.parentNode.removeChild(launchMeetingDiv.parentNode)
        } else {
            console.error("Launch meeting button not found")
        }
    } catch (e) {
        console.error("Cant auto click on 'Launch meeting' button.")
        console.error(e)
    }
}

/**
 * Increase size of 'Join from browser' link.
 */
function increaseSizeOfRedirectLink() {
    setTimeout(function () {
        try {
            let allA = document.getElementsByTagName("a")
            let joinFromBrowserA

            for (let a of allA) {
                if (JOIN_FROM_BROWSER_LINK_TEXT.some(text => a.textContent === text)) {
                    joinFromBrowserA = a
                    console.debug("'Join from browser' link found")
                    break
                }
            }

            if (joinFromBrowserA !== undefined) {
                joinFromBrowserA.style.fontSize = "xx-large"
            } else {
                console.error("'Join from browser' link not found")
            }
        } catch (e) {
            console.error("Cant increase size of 'Join from browser' link.")
            console.error(e)
        }
    }, 1000)
}

/**
 * Increase for each user time counter if user was talk.
 */
function timeProcessing() {
    setInterval(function () {
        let participantsItems = document.getElementsByClassName("participants-item__display-name")

        for (let participant of participantsItems) {
            participant.parentElement.style.display = "block"

            let participantName = participant.innerHTML
            if (userTimeMap[participantName] === undefined) {
                userTimeMap[participantName] = 0
                continue
            }

            let isTimeSpanCreated = false
            for (let childNode of participant.parentElement.children) {
                if (childNode.hasAttribute("time")) {
                    isTimeSpanCreated = true
                    break
                }
            }
            if (!isTimeSpanCreated) {
                let timeSpan = document.createElement("span")
                timeSpan.setAttribute("time", "0")
                timeSpan.style.float = "right"
                participant.parentElement.appendChild(timeSpan)
            }

            let statusIcon = participant.parentElement.parentElement.parentElement.getElementsByTagName("i")[0]
            if (statusIcon.className === "participants-icon__participants-mute-animation") {
                userTimeMap[participantName]++
            }

            for (let childNode of participant.parentElement.children) {
                if (childNode.hasAttribute("time")) {
                    childNode.textContent = userTimeMap[participantName]
                }
            }

            participant.parentElement.nodeValue = userTimeMap[participantName]
        }
    }, 1000)
}